<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use App\Models\Author;
use App\Models\Book;
use App\Models\File;

class BooksController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // Filters
        $where = [];
        if (!empty($request->id_editorial)) $where[] = ['id_editorial', $request->id_editorial];
        if (!empty($request->title)) $where[] = ['title', 'LIKE', "%".$request->title."%"];
        if (!empty($request->id)) $where[] = ['id', $request->id];

        $books = Book::where($where)->get();

        $resp = [];
        foreach ($books as $key => $value) {
            $book = $value->toArray();

            // Get editorial name
            $editorial = $value->editorial()->get()->toArray();
            if (!empty($editorial)) {
                $book['editorial'] = $editorial[0]['name'];
            }

            // Get author name and file
            $booksAuthors = $value->booksAuthors()->get()->toArray();
            if (!empty($booksAuthors)) {
                $author_file = File::where('id_author', $book['id'])->first();
                $author = Author::find($booksAuthors[0]['id_author']);

                $book['author_file'] = $author_file->name;
                $book['author'] = $author->name;
            }

            // Get file name
            $file = File::where('id_book', $book['id'])->first();
            if (!empty($file)) {
                $book['file'] = $file->name;
            }

            $resp[] = $book;
        }

        return $resp;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            // Validations
            $valid_book = Validator::make(
                $request->all(),
                [
                    'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                    'publish_at' => 'required|date',
                    'id_editorial' => 'required',
                    'title' => 'required|max:50',
                ]
            );
            if ($valid_book->fails()) {
                return response()->json([
                    'errors' => $valid_book->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $book = Book::create([
                'id_editorial' => $request->id_editorial,
                'publish_at' => $request->publish_at,
                'price' => $request->price,
                'title' => $request->title,
            ]);

            return response()->json([
                'message' => 'Book created',
                'book' => $book,
                'status' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Update a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            // Validations
            $valid_book = Validator::make(
                $request->all(),
                [
                    'id' => 'required'
                ]
            );
            if ($valid_book->fails()) {
                return response()->json([
                    'errors' => $valid_book->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $update = [];
            if (!empty($request->id_editorial)) $update['id_editorial'] = $request->id_editorial;
            if (!empty($request->publish_at)) $update['publish_at'] = $request->publish_at;
            if (!empty($request->price)) $update['price'] = $request->price;
            if (!empty($request->title)) $update['title'] = $request->title;

            return Book::where('id', $request->id)
                ->update($update);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $book = Book::find($id);
        $book->delete();
    }
}

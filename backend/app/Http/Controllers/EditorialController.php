<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\Editorial;

class EditorialController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // Filters
        $where = [];
        if (!empty($request->name)) $where[] = ['name', 'LIKE', "%".$request->name."%"];
        if (!empty($request->id)) $where[] = ['id', $request->id];

        if (!empty($where)) {
            return Book::where($where)->get();
        }

        return Editorial::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            // Validations
            $valid_editorial = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required|max:10',
                    'direccion' => 'required|max:100',
                    'name' => 'required|max:50',
                ]
            );
            if ($valid_editorial->fails()) {
                return response()->json([
                    'errors' => $valid_editorial->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $editorial = Editorial::create([
                'phone_number' => $request->phone_number,
                'direccion' => $request->direccion,
                'name' => $request->name,
            ]);

            return response()->json([
                'message' => 'Editorial created',
                'editorial' => $editorial,
                'status' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Update a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            // Validations
            $valid_editorial = Validator::make(
                $request->all(),
                [
                    'id' => 'required'
                ]
            );
            if ($valid_editorial->fails()) {
                return response()->json([
                    'errors' => $valid_editorial->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $update = [];
            if (!empty($request->phone_number)) $update['phone_number'] = $request->phone_number;
            if (!empty($request->direccion)) $update['direccion'] = $request->direccion;
            if (!empty($request->name)) $update['name'] = $request->name;

            return Editorial::where('id', $request->id)
                ->update($update);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $editorial = Editorial::find($id);
        $editorial->delete();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\File;

class FileController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // Filters
        $where = [];
        if (!empty($request->id_author)) $where[] = ['id_author', $request->id_author];
        if (!empty($request->id_book)) $where[] = ['id_book', $request->id_book];

        if (!empty($where)) {
            return File::where($where)->get();
        }

        return File::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            // Validations
            $valid_file = Validator::make(
                $request->all(),
                [
                    'id_author' => 'required_without:id_book',
                    'id_book' => 'required_without:id_author',
                    'name' => 'required|max:50',
                ]
            );
            if ($valid_file->fails()) {
                return response()->json([
                    'errors' => $valid_file->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $file = File::create([
                'id_author' => $request->id_author,
                'id_book' => $request->id_book,
                'name' => $request->name,
            ]);

            return response()->json([
                'message' => 'File created',
                'status' => true,
                'file' => $file,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Update a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            // Validations
            $valid_file = Validator::make(
                $request->all(),
                [
                    'id' => 'required'
                ]
            );
            if ($valid_file->fails()) {
                return response()->json([
                    'errors' => $valid_file->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $update = [];
            if (!empty($request->id_author)) $update['id_author'] = $request->id_author;
            if (!empty($request->id_book)) $update['id_book'] = $request->id_book;
            if (!empty($request->name)) $update['name'] = $request->name;

            return File::where('id', $request->id)
                ->update($update);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $file = File::find($id);
        $file->delete();
    }
}

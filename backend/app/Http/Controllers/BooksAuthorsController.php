<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\BooksAuthors;

class BooksAuthorsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // Filters
        $where = [];
        if (!empty($request->id_author)) $where[] = ['id_author', $request->id_author];
        if (!empty($request->id_book)) $where[] = ['id_book', $request->id_book];

        if (!empty($where)) {
            return BooksAuthors::where($where)->get();
        }

        return BooksAuthors::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            // Validations
            $valid_books_authors = Validator::make(
                $request->all(),
                [
                    'id_author' => 'required',
                    'id_book' => 'required',
                ]
            );
            if ($valid_books_authors->fails()) {
                return response()->json([
                    'errors' => $valid_books_authors->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $books_authors = BooksAuthors::create([
                'id_author' => $request->id_author,
                'id_book' => $request->id_book,
            ]);

            return response()->json([
                'message' => 'BooksAuthors created',
                'books_authors' => $books_authors,
                'status' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        return BooksAuthors::where([
            ['id_author', $request->id_author],
            ['id_book', $request->id_book],
        ])->delete();
    }
}

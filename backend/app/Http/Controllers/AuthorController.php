<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\Author;

class AuthorController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // Filters
        $where = [];
        if (!empty($request->last_name)) $where[] = ['last_name', 'LIKE', "%".$request->last_name."%"];
        if (!empty($request->name)) $where[] = ['name', 'LIKE', "%".$request->name."%"];
        if (!empty($request->id)) $where[] = ['id', $request->id];

        if (!empty($where)) {
            return Author::where($where)->get();
        }

        return Author::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            // Validations
            $valid_author = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email|max:100',
                    'last_name' => 'required|max:50',
                    'name' => 'required|max:50',
                ]
            );
            if ($valid_author->fails()) {
                return response()->json([
                    'errors' => $valid_author->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $author = Author::create([
                'last_name' => $request->last_name,
                'email' => $request->email,
                'name' => $request->name,
            ]);

            return response()->json([
                'message' => 'Author created',
                'author' => $author,
                'status' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Update a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            // Validations
            $valid_author = Validator::make(
                $request->all(),
                [
                    'id' => 'required'
                ]
            );
            if ($valid_author->fails()) {
                return response()->json([
                    'errors' => $valid_author->errors(),
                    'message' => 'Validation error',
                    'status' => false,
                ], 401);
            }

            $update = [];
            if (!empty($request->last_name)) $update['last_name'] = $request->last_name;
            if (!empty($request->email)) $update['email'] = $request->email;
            if (!empty($request->name)) $update['name'] = $request->name;

            return Author::where('id', $request->id)
                ->update($update);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
                'status' => false,
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $author = Author::find($id);
        $author->delete();
    }
}

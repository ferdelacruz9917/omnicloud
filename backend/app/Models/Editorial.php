<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $direccion
 * @property string $phone_number
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Book[] $books
 */
class Editorial extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'editorial';

    /**
     * @var array
     */
    protected $fillable = ['name', 'direccion', 'phone_number', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books()
    {
        return $this->hasMany(Book::class, 'id_editorial');
    }
}

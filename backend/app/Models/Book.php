<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_editorial
 * @property string $title
 * @property string $publish_at
 * @property float $price
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Editorial $editorial
 * @property BooksAuthors[] $booksAuthors
 */
class Book extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'book';

    /**
     * @var array
     */
    protected $fillable = ['id_editorial', 'title', 'publish_at', 'price', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editorial()
    {
        return $this->belongsTo(Editorial::class, 'id_editorial');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function booksAuthors()
    {
        return $this->hasMany(BooksAuthors::class, 'id_book');
    }
}

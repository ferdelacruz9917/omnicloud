<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_book
 * @property integer $id_author
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class File extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'file';

    /**
     * @var array
     */
    protected $fillable = ['id_book', 'id_author', 'name', 'created_at', 'updated_at', 'deleted_at'];
}

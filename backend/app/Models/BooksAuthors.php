<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_author
 * @property integer $id_book
 * @property string $created_at
 * @property Author $author
 * @property Book $book
 */
class BooksAuthors extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_author', 'id_book', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class, 'id_author');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class, 'id_book');
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

use App\Http\Controllers\BooksAuthorsController;
use App\Http\Controllers\EditorialController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\FileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Users
Route::post('/register', [AuthController::class, 'createUser']);
Route::put('/update', [AuthController::class, 'updateUser']);
Route::get('/login', [AuthController::class, 'loginUser']);

// Author
Route::delete('/author/{id}', [AuthorController::class, 'destroy'])->middleware('auth:sanctum');
Route::post('/author', [AuthorController::class, 'store'])->middleware('auth:sanctum');
Route::put('/author', [AuthorController::class, 'update'])->middleware('auth:sanctum');
Route::get('/author', [AuthorController::class, 'index'])->middleware('auth:sanctum');

// Books authors
Route::delete('/books-authors', [BooksAuthorsController::class, 'destroy'])->middleware('auth:sanctum');
Route::post('/books-authors', [BooksAuthorsController::class, 'store'])->middleware('auth:sanctum');
Route::get('/books-authors', [BooksAuthorsController::class, 'index'])->middleware('auth:sanctum');

// Books
Route::delete('/books/{id}', [BooksController::class, 'destroy'])->middleware('auth:sanctum');
Route::post('/books', [BooksController::class, 'store'])->middleware('auth:sanctum');
Route::put('/books', [BooksController::class, 'update'])->middleware('auth:sanctum');
Route::get('/books', [BooksController::class, 'index'])->middleware('auth:sanctum');

// Editorial
Route::delete('/editorial/{id}', [EditorialController::class, 'destroy'])->middleware('auth:sanctum');
Route::post('/editorial', [EditorialController::class, 'store'])->middleware('auth:sanctum');
Route::put('/editorial', [EditorialController::class, 'update'])->middleware('auth:sanctum');
Route::get('/editorial', [EditorialController::class, 'index'])->middleware('auth:sanctum');

// Files
Route::delete('/files/{id}', [FileController::class, 'destroy'])->middleware('auth:sanctum');
Route::get('/download', [FileController::class, 'download'])->middleware('auth:sanctum');
Route::post('/files', [FileController::class, 'store'])->middleware('auth:sanctum');
Route::put('/files', [FileController::class, 'update'])->middleware('auth:sanctum');
Route::get('/files', [FileController::class, 'index'])->middleware('auth:sanctum');



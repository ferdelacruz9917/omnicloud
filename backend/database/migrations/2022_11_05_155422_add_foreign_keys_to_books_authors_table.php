<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToBooksAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books_authors', function (Blueprint $table) {
            $table->foreign(['id_author'], 'FK_author')->references(['id'])->on('author')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['id_book'], 'FK_book')->references(['id'])->on('book')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books_authors', function (Blueprint $table) {
            $table->dropForeign('FK_author');
            $table->dropForeign('FK_book');
        });
    }
}

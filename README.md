# Fernando De La Cruz - Omnicloud

# Base de datos
En el archivo ``` db.sql ``` se encuentran las sentencias SQL para crear la base de datos, las tablas y los registros. Favor de correrlo en el servidor de base de datos.

# Backend
En la carpeta *backend* se encuentra el proyecto de Laravel. Es necesario tener *composer* para iniciarlo:

``` cd backend ```

``` composer install ```

Después es necesario copiar el archivo *.env.prod* y renombrarlo a *.env*

``` mv .env.prod .env ```

Cambiar los siguientes valores por los del host actual en el archivo *.env*:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=omnicloud
DB_USERNAME=root
DB_PASSWORD=
```
Iniciar el servidor con:

``` php artisan serve ```

# Frontend
En la carpeta *frontend* se encuentra el proyecto de Vue JS. Es necesario tener *npm* para iniciarlo.

` cd front `

` npm install `

Después es necesario copiar el archivo *.env.prod* y renombrarlo a *.env*

``` mv .env.prod .env ```

Cambiar los siguientes valores por los del host actual en el archivo *.env*:

``` VITE_URL=http://127.0.0.1:8000/ ```

Iniciar la aplicación:

``` npm run dev ```

# Postman collection
El archivo *Omnicloud.postman_collection.json* contiene llamadas a la API para hacer pruebas.

Existen 2 variables necesarias para poder hacer las pruebas:

``` URL=http://127.0.0.1:8000 ```


``` TOKEN=2|7HSzRrvjFIPUeduZK36cvbWyehPC3jeyAlKwks53 ```

Estas variables ya están pre configuradas, si necesita actualizarlas las puede cambiar desde el apartado *Variables* en la carpeta de Postman.


# Muchas gracias

Muchas gracias por el interés en mi perfil y por el tiempo invertido. Cualquier dudas o aclaración favor de hacérmela saber. Saludos
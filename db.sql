-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for omnicloud
CREATE DATABASE IF NOT EXISTS `omnicloud` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `omnicloud`;

-- Dumping structure for table omnicloud.author
CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table omnicloud.author: ~10 rows (approximately)
INSERT INTO `author` (`id`, `name`, `last_name`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Autor 1', 'Apellido 1', 'author1@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(2, 'Autor 2', 'Apellido 2', 'author2@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(3, 'Autor 3', 'Apellido 3', 'author3@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(4, 'Autor 4', 'Apellido 4', 'author4@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(5, 'Autor 5', 'Apellido 5', 'author5@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(6, 'Autor 6', 'Apellido 6', 'author6@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(7, 'Autor 7', 'Apellido 7', 'author7@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(8, 'Autor 8', 'Apellido 8', 'author8@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(9, 'Autor 9', 'Apellido 9', 'author9@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL),
	(10, 'Autor 10', 'Apellido 10', 'author10@omnicloud.com', '2022-11-08 16:05:12', '2022-11-08 16:23:49', NULL);

-- Dumping structure for table omnicloud.book
CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_editorial` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT 0.00,
  `publish_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_editorial` (`id_editorial`),
  CONSTRAINT `FK_editorial` FOREIGN KEY (`id_editorial`) REFERENCES `editorial` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table omnicloud.book: ~10 rows (approximately)
INSERT INTO `book` (`id`, `id_editorial`, `title`, `price`, `publish_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Libro 1', 100.00, '2022-11-09 10:32:48', '2022-11-08 17:52:49', '2022-11-08 17:56:24', NULL),
	(2, 2, 'Libro 2', 200.00, '2022-11-09 10:35:21', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(3, 3, 'Libro 3', 300.00, '2022-11-09 10:35:50', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(4, 4, 'Libro 4', 400.00, '2022-11-09 10:36:15', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(5, 5, 'Libro 5', 500.00, '2022-11-09 10:36:26', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(6, 6, 'Libro 6', 600.00, '2022-11-09 10:36:26', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(7, 7, 'Libro 7', 700.00, '2022-11-09 10:36:26', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(8, 8, 'Libro 8', 800.00, '2022-11-09 10:36:26', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(9, 9, 'Libro 9', 900.00, '2022-11-09 10:36:26', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL),
	(10, 10, 'Libro 10', 1000.00, '2022-11-09 10:37:34', '2022-11-08 17:53:18', '2022-11-08 17:53:18', NULL);

-- Dumping structure for table omnicloud.books_authors
CREATE TABLE IF NOT EXISTS `books_authors` (
  `id_author` int(11) NOT NULL,
  `id_book` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  UNIQUE KEY `id_book_unique` (`id_book`),
  KEY `id_aithor` (`id_author`),
  KEY `id_book` (`id_book`),
  CONSTRAINT `FK_author` FOREIGN KEY (`id_author`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table omnicloud.books_authors: ~10 rows (approximately)
INSERT INTO `books_authors` (`id_author`, `id_book`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '2022-11-08 18:09:40', '2022-11-08 18:09:40', NULL),
	(2, 2, '2022-11-08 18:09:40', '2022-11-08 18:09:40', NULL),
	(3, 3, '2022-11-09 16:38:15', '2022-11-09 16:38:15', NULL),
	(4, 4, '2022-11-09 16:38:20', '2022-11-09 16:38:20', NULL),
	(5, 5, '2022-11-09 16:38:24', '2022-11-09 16:38:24', NULL),
	(6, 6, '2022-11-09 16:38:29', '2022-11-09 16:38:29', NULL),
	(7, 7, '2022-11-09 16:38:33', '2022-11-09 16:38:33', NULL),
	(8, 8, '2022-11-09 16:38:37', '2022-11-09 16:38:37', NULL),
	(9, 9, '2022-11-09 16:38:41', '2022-11-09 16:38:41', NULL),
	(10, 10, '2022-11-09 16:38:48', '2022-11-09 16:38:48', NULL);

-- Dumping structure for table omnicloud.editorial
CREATE TABLE IF NOT EXISTS `editorial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table omnicloud.editorial: ~10 rows (approximately)
INSERT INTO `editorial` (`id`, `name`, `direccion`, `phone_number`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Editorial 1', 'Dirección 1', '3213213211', '2022-11-08 17:45:30', '2022-11-08 18:15:56', NULL),
	(2, 'Editorial 2', 'Dirección 2', '3213213212', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(3, 'Editorial 3', 'Dirección 3', '3213213213', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(4, 'Editorial 4', 'Dirección 4', '3213213214', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(5, 'Editorial 5', 'Dirección 5', '3213213215', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(6, 'Editorial 6', 'Dirección 6', '3213213216', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(7, 'Editorial 7', 'Dirección 7', '3213213217', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(8, 'Editorial 8', 'Dirección 8', '3213213218', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(9, 'Editorial 9', 'Dirección 9', '3213213219', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL),
	(10, 'Editorial 10', 'Dirección 10', '3213213210', '2022-11-08 17:45:56', '2022-11-08 17:45:56', NULL);

-- Dumping structure for table omnicloud.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table omnicloud.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table omnicloud.file
CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_book` int(11) DEFAULT NULL,
  `id_author` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_author` (`id_author`),
  KEY `id_book` (`id_book`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table omnicloud.file: ~20 rows (approximately)
INSERT INTO `file` (`id`, `id_book`, `id_author`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, 'Give.pdf', '2022-11-08 18:16:28', '2022-11-08 18:16:28', NULL),
	(2, 2, NULL, 'RH_StudyGuide_V2.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(3, 3, NULL, 'Start.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(4, 4, NULL, 'The_Explosive.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(5, 5, NULL, 'The_Gifts.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(6, 6, NULL, 'Give.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(7, 7, NULL, 'RH_StudyGuide_V2.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(8, 8, NULL, 'Start.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(9, 9, NULL, 'The_Explosive.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(10, 10, NULL, 'The_Gifts.pdf', '2022-11-08 18:17:03', '2022-11-08 18:17:03', NULL),
	(11, NULL, 1, 'author1.jpg', '2022-11-09 16:47:14', '2022-11-09 16:47:14', NULL),
	(12, NULL, 2, 'author2.jpg', '2022-11-09 16:47:26', '2022-11-09 16:47:26', NULL),
	(13, NULL, 3, 'author3.jpg', '2022-11-09 16:47:31', '2022-11-09 16:47:31', NULL),
	(14, NULL, 4, 'author4.jpg', '2022-11-09 16:47:38', '2022-11-09 16:47:38', NULL),
	(15, NULL, 5, 'author5.jpg', '2022-11-09 16:47:44', '2022-11-09 16:47:44', NULL),
	(16, NULL, 6, 'author6.jpg', '2022-11-09 16:47:50', '2022-11-09 16:47:50', NULL),
	(17, NULL, 7, 'author7.jpg', '2022-11-09 16:47:56', '2022-11-09 16:47:56', NULL),
	(18, NULL, 8, 'author8.jpg', '2022-11-09 16:48:01', '2022-11-09 16:48:01', NULL),
	(19, NULL, 9, 'author9.jpg', '2022-11-09 16:48:12', '2022-11-09 16:48:12', NULL),
	(20, NULL, 10, 'author10.jpg', '2022-11-09 16:48:23', '2022-11-09 16:48:23', NULL);

-- Dumping structure for table omnicloud.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table omnicloud.migrations: ~11 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2022_11_05_155421_create_author_table', 0),
	(2, '2022_11_05_155421_create_book_table', 0),
	(3, '2022_11_05_155421_create_books_authors_table', 0),
	(4, '2022_11_05_155421_create_editorial_table', 0),
	(5, '2022_11_05_155421_create_file_table', 0),
	(6, '2022_11_05_155422_add_foreign_keys_to_book_table', 0),
	(7, '2022_11_05_155422_add_foreign_keys_to_books_authors_table', 0),
	(8, '2014_10_12_000000_create_users_table', 1),
	(9, '2014_10_12_100000_create_password_resets_table', 1),
	(10, '2019_08_19_000000_create_failed_jobs_table', 1),
	(11, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- Dumping structure for table omnicloud.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table omnicloud.password_resets: ~0 rows (approximately)

-- Dumping structure for table omnicloud.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table omnicloud.personal_access_tokens: ~3 rows (approximately)
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Models\\User', 1, 'API TOKEN', 'a82f627d14015f502099db9ed7b0af70ac29260865832632a0420338df8b4c4a', '["*"]', NULL, NULL, '2022-11-08 21:41:07', '2022-11-08 21:41:07'),
	(2, 'App\\Models\\User', 1, 'API TOKEN', '2caa42b1e15a6668eb3f4609c41ad3c889250575da2a2aeb9bd2f3e45db038a5', '["*"]', '2022-11-09 22:48:23', NULL, '2022-11-08 21:53:06', '2022-11-09 22:48:23'),
	(3, 'App\\Models\\User', 1, 'API TOKEN', 'd85b2b3d7a0816e40cd6b27921d5f8faafd1ce838a5673fa1847ebe124ef29bc', '["*"]', NULL, NULL, '2022-11-08 22:22:57', '2022-11-08 22:22:57'),
	(4, 'App\\Models\\User', 1, 'API TOKEN', '8c447f1bb425b3cb4da600455eeef8bc582a2776974a53a94a576b2bf4ac44fd', '["*"]', NULL, NULL, '2022-11-09 05:08:01', '2022-11-09 05:08:01'),
	(5, 'App\\Models\\User', 1, 'API TOKEN', 'e487bd2146db466c44e74b77920e9b5177968b3c2cc2f73799696f62c310c882', '["*"]', NULL, NULL, '2022-11-09 05:16:14', '2022-11-09 05:16:14'),
	(6, 'App\\Models\\User', 1, 'API TOKEN', '70827ad165c5260248ae34811a9dbb39c76e48d43dd1f3ad89d4ae2282f10635', '["*"]', NULL, NULL, '2022-11-09 05:17:24', '2022-11-09 05:17:24'),
	(7, 'App\\Models\\User', 1, 'API TOKEN', 'b334c6d1a4719445f074195f5e0661645541aec62c266930832597c7a4147b2e', '["*"]', NULL, NULL, '2022-11-09 05:19:23', '2022-11-09 05:19:23'),
	(8, 'App\\Models\\User', 1, 'API TOKEN', '4cc50dd26a42a52ccb4070e32713e7c89fe72675ffbca29a2e113d4c7822142b', '["*"]', NULL, NULL, '2022-11-09 05:19:44', '2022-11-09 05:19:44'),
	(9, 'App\\Models\\User', 1, 'API TOKEN', 'bfaa8d4ca7baf5221934e62e1fd4afe2fd342e788193418fbabbb534c053132e', '["*"]', NULL, NULL, '2022-11-09 05:20:32', '2022-11-09 05:20:32'),
	(10, 'App\\Models\\User', 1, 'API TOKEN', 'a14d11a748ce5e50f3efa2cca56793a920609e340f2bbe69b09e05e526af1ba8', '["*"]', NULL, NULL, '2022-11-09 05:20:58', '2022-11-09 05:20:58'),
	(11, 'App\\Models\\User', 1, 'API TOKEN', '07a7242483ef49cb88ced1ba7ae44f8f4b315f32eb932950e3d3658e88f88cc8', '["*"]', NULL, NULL, '2022-11-09 05:21:16', '2022-11-09 05:21:16'),
	(12, 'App\\Models\\User', 1, 'API TOKEN', 'f063f6387568bd7b9a14ce5cdf8d1c54a19885fd232eeeb58630acd8ca81dfd2', '["*"]', NULL, NULL, '2022-11-09 05:23:47', '2022-11-09 05:23:47'),
	(13, 'App\\Models\\User', 1, 'API TOKEN', 'e11a946f27d4596a4ae2aa89485de03d2f05990962846dd49e9cf785111fba7a', '["*"]', NULL, NULL, '2022-11-09 05:24:13', '2022-11-09 05:24:13'),
	(14, 'App\\Models\\User', 1, 'API TOKEN', 'e471dcab47bb30ed1b2080ad9091c60f82d23b826da4a59c6e06057c6584a67f', '["*"]', NULL, NULL, '2022-11-09 05:24:40', '2022-11-09 05:24:40'),
	(15, 'App\\Models\\User', 1, 'API TOKEN', '6826b60704f413ea33718cd56d8a0589d3207337deeb387c27c55f6f1390652c', '["*"]', NULL, NULL, '2022-11-09 05:24:57', '2022-11-09 05:24:57'),
	(16, 'App\\Models\\User', 1, 'API TOKEN', '1cafe8dd4ff5119a062e7681097c41908cae89816845f7a67bcc5dc01670db05', '["*"]', NULL, NULL, '2022-11-09 05:25:48', '2022-11-09 05:25:48'),
	(17, 'App\\Models\\User', 1, 'API TOKEN', 'f8b78fba0eb9c4488790974f34b3d2c329ec14dc515bb4abced1c84f1be6a4b8', '["*"]', NULL, NULL, '2022-11-09 05:31:45', '2022-11-09 05:31:45'),
	(18, 'App\\Models\\User', 1, 'API TOKEN', '5c5338fa2c94d3bcf0c68fc5cb1892486f2b7d0906a4044afbe2436b88a30c2f', '["*"]', NULL, NULL, '2022-11-09 05:32:22', '2022-11-09 05:32:22'),
	(19, 'App\\Models\\User', 1, 'API TOKEN', 'cf27e058485c9da9b29b0f4cc1b93c8f041461dae7fbd4ba044bc88ccc6f44a5', '["*"]', NULL, NULL, '2022-11-09 05:32:48', '2022-11-09 05:32:48'),
	(20, 'App\\Models\\User', 1, 'API TOKEN', 'f388aae60b73a79be19a916ea913402b92d6e9ea1d712569bf011c0be2c875ab', '["*"]', NULL, NULL, '2022-11-09 05:36:43', '2022-11-09 05:36:43'),
	(21, 'App\\Models\\User', 1, 'API TOKEN', '3293f8fd4856cf70696373ab82b98d99a15e3cf4eb8235d3de547c7e0f92808c', '["*"]', '2022-11-09 23:09:45', NULL, '2022-11-09 20:19:37', '2022-11-09 23:09:45');

-- Dumping structure for table omnicloud.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table omnicloud.users: ~1 rows (approximately)
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$FoV4GgK1kh7O3HbCoAl7jO6bcmxcJjTCTzhD.XJSN5bloB7zUAr4e', NULL, '2022-11-08 21:41:07', '2022-11-08 21:47:47');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

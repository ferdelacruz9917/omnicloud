import axios from './plugins/axios'
import { createApp } from 'vue'
import App from './App.vue'


const app = createApp(App)

app.use(axios, {
    url: import.meta.env.VITE_URL,
})

app.mount('#app')
